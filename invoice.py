# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, Equal, Not, And

__all__ = ['InvoiceLine']


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    purchase_reference = fields.Function(
        fields.Char('Purchase Reference'),
        'get_purchase_data', searcher='search_purchase_data')
    purchase_date = fields.Function(
        fields.Date('Purchase date'),
        'get_purchase_data', searcher='search_purchase_data')

    def get_purchase_data(self, name=None):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')

        _name = name if name == 'purchase_date' else name[9:]
        if isinstance(self.origin, PurchaseLine):
            return getattr(self.origin.purchase, _name)

    @classmethod
    def search_purchase_data(cls, name, clause):
        _name = clause[0] if clause[0].startswith('purchase_date') \
            else clause[0][9:]
        return [('origin.purchase.%s' % _name,) +
            tuple(clause[1:3]) + ('purchase.line',) + tuple(clause[3:])]

    @classmethod
    def view_attributes(cls):
        return super(InvoiceLine, cls).view_attributes() + [
            ('/tree/field[@name="purchase_reference"]', 'tree_invisible',
                And(Not(Equal(Eval('invoice_type', ''), 'in')),
                Not(Equal(Eval('type', ''), 'in')))),
            ('/tree/field[@name="purchase_date"]', 'tree_invisible',
                And(Not(Equal(Eval('invoice_type', ''), 'in')),
                Not(Equal(Eval('type', ''), 'in')))),
            ]
